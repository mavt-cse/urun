#! /bin/sh

B=/scratch/snx3000/amlucas/HeleShaw/84

GG="128.0 24.0 32.0 56.0 64.0 72.0 96.0 112.0 144.0 156.0 172.0"

LX=768
LY=128
LZ=8

for g in $GG; do
    SS=`ls ${B}/${g},8.0/bop/solvent*0.bop`
    CC=`ls ${B}/${g},8.0/bop/color*0.bop`
    echo $g
    ./post/growths $LX $LY $LZ $SS -- $CC > profile-$g.txt    
done
