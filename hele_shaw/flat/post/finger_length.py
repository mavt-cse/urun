#!/usr/bin/python2

import sys
import numpy as np

tol = 0.05

def usg():
    print "usg: finger_length.py <prifile.txt>"
    exit(1)

def read(name):
    D = np.loadtxt(name)
    x = D[:,0]
    p = D[:,3]
    return x, p

def min_finger(xx, pp):
    for (x, p) in zip(xx, pp):
        if p < 1-tol:
            return x
    return xx[-1]

def max_finger(xx, pp):
    for (x, p) in reversed(zip(xx, pp)):
        if p > tol:
            return x
    return xx[0]

def ratio(Li, Lo):
    Lf = Lo - Li
    if Li <= 1e-6:
        return 0
    return Lf / Li

if len(sys.argv) != 2:
    usg()

(xx, pp) = read(sys.argv[1])

Li = min_finger(xx, pp);
Lo = max_finger(xx, pp);

#print Li, Lo
print ratio(Li, Lo)
