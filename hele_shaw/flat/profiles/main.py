#! /usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
import sys
from scipy.optimize import minimize

def load_data(f):
    r = np.loadtxt(f)
    t = np.array(range(len(r)))
    return t, r

def filter(t, r):
    e = r[-1]
    T = 0.1 * e;
    i = len(t) - 1
    while r[i] > T:
        i -= 1
    return t[i:], r[i:]

def scale_critical(t, r, ts, tc, rc):
    return (t - ts) * 1.0 / tc, r * 1.0 / rc

def d2(x, y, x0, y0):
    return (x-x0)**2 + (y-y0)**2

def d2min(x, y, xx, yy):
    dd_ = [d2(x, y, x0, y0) for (x0, y0) in zip(xx, yy)]
    return min(dd_)

def dist(tw, rw, t0, r0):
    dd = [d2min(x, y, t0, r0) for (x, y) in zip(tw, rw)]
    return sum(dd)

def distance(x):
    # work
    tw, rw = scale_critical(t, r, x[0], x[1], x[2])
    return dist(tw, rw, t0, r0)

def fit_critical(t0, r0, t, r):
    # res = minimize(distance, [0.0, 1.0, 1.0], method='nelder-mead', options={'xtol': 1e-8, 'disp': True})
    # return res.x[0], res.x[1], res.x[2]
    return 0.0, 1.0, 1.0


fnames = sys.argv[1:]

fig = plt.figure(0)
ax = fig.add_subplot(1, 1, 1)

id = 0
tts = []
ttc = []
rrc = []

for f in fnames:
    (t, r) = load_data(f)
    (t, r) = filter(t, r)
    if id == 0:
        t0 = t
        r0 = r
        ts, tc, rc = (0.0, 1.0, 1.0)
    else:
        (ts, tc, rc) = fit_critical(t0, r0, t, r)
    t, r = scale_critical(t, r, ts, tc, rc)
    tts.append(ts)
    ttc.append(tc)
    rrc.append(rc)
    ax.plot(t, r, label = f)
    id += 1

ax.grid()
#ax.set_xscale('log')
#ax.set_yscale('log')
#ax.legend()
plt.xlabel("t")
plt.ylabel("r")
plt.show()
print ttc
print rrc
