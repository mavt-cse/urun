#!/usr/bin/python2

import sys
import numpy as np

def usg():
    print "Usage:", sys.argv[0], "<Lz> <profile.txt>"
    exit(1)

def read(fname):
    d = np.loadtxt(fname)
    z = d[:,2] # z coordinates  
    s = d[:,5] # stress s_xz
    # make cell centers
    dz = z[1] - z[0]
    z += dz / 2
    
    return z, s

def filter_walls(L, z, s):
    H = 0.5 * L
    ii = np.argwhere(abs(z - H) < H - 1)
    s = s[ii]
    z = z[ii]
    return z, s


if len(sys.argv) != 3:
    usg()

Lz = int(sys.argv[1])
(z, s) = read(sys.argv[2])
(z, s) = filter_walls(Lz, z, s)

print np.mean(s)
