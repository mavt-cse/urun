#!/usr/bin/python2

import sys
import numpy as np
# import matplotlib.pyplot as plt

def usg():
    print "Usage:", sys.argv[0], "<profile.txt>"
    exit(1)

def read(fname):
    d = np.loadtxt(fname)
    y = d[:,1]    
    u = d[:,3]
    # make cell centers
    dy = y[1] - y[0]
    y += dy / 2
    u -= np.mean(u)
    return y, u

def fit(y, u):
    n = len(y)
    ya = y[n/2:]
    ua = 0.5 * (abs(u[:n/2]) + abs(u[n/2:]))
    
    coeff = np.polyfit(ya, ua, 2)
    U  = np.poly1d(coeff)

    # find max
    dU    = np.polyder(U)
    roots = np.roots(dU)
    umax = U(roots[0])
    return umax, ya, U(ya)
    

if len(sys.argv) != 2:
    usg()

(y, u) = read(sys.argv[1])
(umax, ya, ua) = fit(y, u)

print umax

# plt.plot(y, u, '+')
# plt.plot(ya, ua, '-')
# plt.show()
