#!/usr/bin/python2

import sys
import numpy as np
# import matplotlib.pyplot as plt

def usg():
    print "Usage:", sys.argv[0], "<profile.txt>"
    exit(1)

def read(fname):
    d = np.loadtxt(fname)
    y = d[:,1] # y coordinates  
    s = d[:,4] # stress s_xy
    # make cell centers
    dy = y[1] - y[0]
    y += dy / 2
    
    return y, s

def fit(y, s):
    n = len(y)

    sl = s[:n/2]
    sr = s[n/2:]
    sr = sr[::-1]
    # average of two subdomains
    ya = y[:n/2]
    sa = 0.5 * (sl + sr)
    
    coeff = np.polyfit(ya, sa, 1)
    S     = np.poly1d(coeff)
    dS    = np.polyder(S)

    return ya, S(ya), dS[0]
    

if len(sys.argv) != 2:
    usg()

(y, s) = read(sys.argv[1])
(ya, sa, dS) = fit(y, s)

print dS
# plt.plot(y, s, '+')
# plt.plot(ya, sa, '-')
# plt.show()
